// random_sample.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <fstream>      // std::ifstream
#include <vector>
#include <random>
#include <time.h>

using namespace std;


vector<string> get_all_files_names(string infile)
{
	vector<string> names;

	ifstream ifs;
	string line;
	string row;


	


	ifs.open(infile.c_str(), ifstream::in);

	if (ifs.is_open())
	{
		while (ifs.good())
		{
			getline(ifs, line);

			if(line.length() > 2)
				names.push_back(line);
			
		}

	}
	ifs.close();

	return names;
}

void sample_filenames(string fout,vector<string> filenames, double fraction) {

	int nout = (int)(fraction*filenames.size());
	ofstream ofs; 
	ofs.open(fout, ios::app);
	vector<int> obs;

	for (int i = 0; i < filenames.size(); i++) obs.push_back(i);

	srand((unsigned int)time(0));
	random_shuffle(obs.begin(), obs.end());

	if (ofs.is_open()) {
		for (int j = 0; j < nout; j++) {
			ofs << filenames[obs[j]];
			ofs << endl;
		}
	}

}

int main(int argc, char* argv[])
{
	
	if (argc < 4) {
		cout << "enter input file file,enter output file file and fraction sampled";
		return -1;
	}

	stringstream convert(argv[3]);

	double fraction;

	convert >> fraction;

	if (fraction > 1.0) {
		cout << "fraction must be >= 1.0";
	}

	string infname(argv[1]);
	string outfname(argv[2]);

	vector<string> files  = get_all_files_names(infname);
	
	sample_filenames(outfname, files, fraction);

    return 0;
}

